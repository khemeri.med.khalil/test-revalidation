export default async function Home() {

  return (
    <>

      <h1>Home Page</h1>

      <p>After conducting several tests, I have found that including more than 58 characters in the [slug] parameter will produce the error:</p>

      <p>I used ?path=understanding-inversion-of-control-dependency-inversion-pr === no problem (58 char)</p>
      <p>I used ?path=understanding-inversion-of-control-dependency-inversion-pri === Error thrown (59 char)</p>

      <p>I created get method to test the revalidatePath()</p>

      <p>Problem detected on vercel</p>
      <p>/api/revalidate?path=</p>


      <div style={{ width: "100%", display: "flex", justifyContent: 'center' }}>

        <img
          src="/screenshot.png"
          style={{
            width: "100%",
            maxWidth: "600px"
          }}
          alt="Picture of the error"
        />
      </div>

    </>

  );
}
