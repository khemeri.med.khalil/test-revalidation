/** Add your relevant code here for the issue to reproduce */
export default async function Home() {
  const posts = (await (await fetch("https://api.jsonbin.io/v3/b/648700e79d312622a36e46cd")).json())?.record;
  return (
    <>
      {posts.map((e) => (
        <div key={e.id}>{e.content}</div>
      ))}
      <div>{JSON.stringify(posts)}</div>
    </>
  );
}
