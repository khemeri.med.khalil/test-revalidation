import { NextRequest, NextResponse } from 'next/server';
import { revalidatePath } from 'next/cache';

export async function GET(request: NextRequest) {
  const path = request.nextUrl.searchParams.get('path') || 'default-post';
  console.log("path: ",path);
  
  // recording to https://nextjs.org/docs/app/api-reference/functions/revalidatePath#parameters
  // cannot revalidate single path as it appears, so no need for path variable
  // revalidatePath("/blog/" + path);

  revalidatePath("/blog/[slug]");
  return NextResponse.json({ revalidated: true, now: Date.now() });
}


// same as https://nextjs.org/docs/app/api-reference/functions/revalidatePath

// After conducting several tests, I have found that including more than 58 characters in the [slug] parameter will produce the error.
//     case 1 :  ?path=understanding-inversion-of-control-dependency-inversion-pr === no problem (58 char)
//     case 2 :  ?path=understanding-inversion-of-control-dependency-inversion-pri === Error thrown (59 char)
