export default async function Post({ params }) {
  return (
    <>
      <h1>Generic Posts Page: /post/[any slug]</h1>
      <h5>Actual Page: /post/{params.slug}</h5>
    </>
  );
}
